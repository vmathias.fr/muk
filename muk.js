"use strict";
const Discord = require ('discord.js');
const config = require("./config.json");
const crypto = require("crypto");
const request = require("xhr-request");
var bot = new Discord.Client();
var m_roles = '/events/index';

bot.on('message',  (message) => {
    //hi : commande de test de bonne connexion du bot
    if (message.content == config.prefix + 'hi') {
        message.reply('Bonjour, je suis en ligne !');
    }

    //events : liste les events à venir
    if (message.content == config.prefix + 'events') {
        let hmac = crypto.createHmac('sha1',config.m_token);
        hmac.update(m_roles);
        const key = hmac.digest('hex');
        const json = `http://51.38.177.106/api/events/index/key:${key}.json`;
        let embed = new Discord.MessageEmbed();
        embed.setTitle('Events à venir');
        embed.setColor('#FF1900');
        request(
            json, {
                method:'GET',
                json : true
            },
            (err,data) => {
                if (data) {
                    for (const events of data.events) {
                        console.log(events,events.Event);
                        embed.addField(events.Event.title, events.Event.time_invitation);
                    }
                    message.channel.send(embed);
                    
                }
            }
        );
    }
});
bot.login(config.token);